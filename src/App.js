import React, { useState } from 'react';
import { v4 as uuid } from "uuid";
import edit from "./imgs/edit.png";
import deleteicon from "./imgs/deleteicon.png"


const App = () => {
  const [todos, setTodos] = useState([]);
  const [newInput, setNewInput] = useState(""); 

  const addFunc = () =>{
    if (newInput){
      let tempTodos = [...todos]
      tempTodos.push({id: uuid(), todo: newInput, state: true})
      setTodos(tempTodos)
      setNewInput("")
    }
  }

  const displayTodos = () => {
    return (
      <ul>
       {todos.map((item,idx) =>
        <li key={item.id}>
          {item.state ? <p className="todoItem">{item.todo}</p> : <p className="todoItem"><del>{item.todo}</del></p>}
          <div className="buttons">  
            <input id="checkbox" type="checkbox" onClick={(e,i)=>toggleFunc(e,idx)} />
            <img alt ="editicon" className="icon" src={edit} onClick={(e,i)=>editFunc(e,idx)}/>
            <img alt="deleteicon" id="deleteicon" className="icon" src={deleteicon} onClick={(e,i)=>deleteFunc(e,idx)} />
          </div>
        </li>)
        }
      </ul>
    )
  }

  const deleteFunc = (event,i) => {
    let tempTodos = [...todos]
    tempTodos.splice(i, 1)
    setTodos(tempTodos)
  }

  const toggleFunc = (event, i) => {
    let tempTodos = [...todos]
    tempTodos[i].state = !tempTodos[i].state
    setTodos(tempTodos)
  }

  const editFunc = (event, i) => {
    var edited = prompt("Enter new todo text")
    if (edited){
      let tempTodos = [...todos]
      tempTodos[i].todo = edited
      setTodos(tempTodos)
    }
  }

  return (
  <div id="main">
    <h1>To-do list</h1>
    {displayTodos()}
    <div id="inputandbutton">
      <input id="input" value={newInput} onKeyPress={(event)=> event.key==="Enter" && addFunc()} onChange={(event)=>{setNewInput(event.target.value)}}/>
      <button id="addbutton" onClick={addFunc}>Add</button>
    </div>
  </div>)
  
}

export default App;

